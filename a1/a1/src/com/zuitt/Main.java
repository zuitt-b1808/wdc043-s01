package com.zuitt;

import  java.util.Scanner;

public class Main {
    public static void main(String[]args){

        Scanner scannerName = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = scannerName.nextLine();

        System.out.println("Last Name:");
        String lastName= scannerName.nextLine();

        System.out.println("First Subject Grade:");
        double firstsubjGrade = scannerName.nextDouble();
        System.out.println("Second Subject Grade:");
        double secondsubjGrade = scannerName.nextDouble();
        System.out.println("Third Subject Grade:");
        double thirdsubjGrade = scannerName.nextDouble();
        double sum = firstsubjGrade + secondsubjGrade + thirdsubjGrade;
        double ave = sum / 3;
        System.out.println("Good day, " + firstName + lastName);
        System.out.println("Your grade average is:" + ave);

    }
}
